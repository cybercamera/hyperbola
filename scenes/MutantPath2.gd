extends Path2D

onready var Map = get_parent()
export var mob_speed = 380

var free_path = false

func _ready():
	randomize()
	$Follow.unit_offset = randf() #we set a random point of location of this mob on the follow path, between 0 & 1
	
func _process(delta):
	$Follow.set_offset($Follow.get_offset() + mob_speed * delta)
