extends TextureRect


func _ready():
	Globals.top_scores.clear()
	MakeFauxTopScores()
	#Globals.SaveScores()
	GetIntroText()
	
	

func _process(delta):
	if Input.is_action_pressed("ui_cancel"):
		_on_Quit_pressed()
	if Input.is_action_pressed("ui_accept"):
		_on_Play_pressed()

func GetIntroText():
	var f = File.new()
	if not f.file_exists("res://intro_text.txt"):
	    print("No intro text file!")
	    return
	
	if f.open("res://intro_text.txt", File.READ) != 0:
		print("Error opening file")
		return
		
	$Description.text = f.get_as_text()
	
func MakeFauxTopScores():
	var top_scores_text
	top_scores_text = Globals.LoadScores()
	if !top_scores_text:
		top_scores_text = "\t\t\t\t\tHall of Fame"
		#There are no top scores, so we make some up and save them.
		Globals.top_scores[0] = ["conz", 1000]
		Globals.top_scores[1] = ["eliana", 900]
		Globals.top_scores[2] = ["eliana", 1900]
		#Globals.SaveScores()
		#top_scores_text = Globals.LoadScores()
		
	$TopScores.text = top_scores_text
	#print(top_scores_text)
#	for i in range(Globals.top_scores.size()):
#		print("Saving scores: " + str(Globals.top_scores.values()[i][0]) + ": " +  str(Globals.top_scores.values()[i][1]))
		#sorted_scores.append(ScoresClass.new(top_scores.values()[i][0], top_scores.values()[i][1]))
		#$ItemList.add_item(str(Globals.top_scores.values()[i][1]))

func _on_Quit_pressed():
	print("_on_Quit_pressed()")
	get_tree().quit()


func _on_Play_pressed():
	$GetPlayerName/PlayerName.grab_focus()
	$GetPlayerName.popup()
	


func _on_PlayerName_text_entered(new_text):
	Globals.player_name = new_text
	_on_GetPlayerName_confirmed()


func _on_GetPlayerName_confirmed():
	Globals.player_name = $GetPlayerName/PlayerName.text
	get_tree().change_scene("res://scenes/Map.tscn")
