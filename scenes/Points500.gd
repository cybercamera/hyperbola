extends Node2D

func _ready():
	self.visible = false

func Strobe():
	print("Strobe()")
	self.visible = true
	$AnimationPlayer.current_animation = "Strobe"
	$AnimationPlayer.play()
	$Timer.start()

func _on_Timer_timeout():
	$AnimationPlayer.stop()
	self.visible = false
