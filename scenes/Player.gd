extends Area2D


onready var LaserBeam = preload("res://scenes/LaserBeam.tscn")
onready var PlayerExplosion = preload("res://scenes/PlayerExplosion.tscn")
onready var NewAstronaut = preload("res://scenes/Astronaut.tscn")
onready var SmartBomb = preload("res://scenes/SmartBomb.tscn")

onready var HUD = get_parent().get_node("HUD")
onready var Map = get_tree().get_root().get_node("Map")

const MAX_ACCELL = 10
const RESPAWN_MAX_COUNT = 10


enum DIRECTION_HEADING {Left = 0, Right = 1}


var SPEED = 100
var velocity = Vector2()

var accel = 10 #Maximal accelleration at start of impulse
var can_fire = true
var hyperjump_out = false
var direction_heading = DIRECTION_HEADING.Right
var player_dying = false
var spawn_phase_in = false
var respawn_countdown = RESPAWN_MAX_COUNT
var astronaut_grabbed = false
var smart_bomb_active = false

var game_over = false

func _ready():
	add_to_group("player")
	Globals.screensize = get_viewport_rect().size
	#print("Globals.screensize: " + str(Globals.screensize))
	$HyperspaceJump.connect("hyperspace_player", self, "_hyperspace_jump_finished")
	$HyperspaceJump.visible = false
	SpawnPlayer()
	
	
func _process(delta):
	if game_over:
		return
	var impulse = Vector2()
	if Input.is_action_pressed("ui_right"):
		impulse.x += accel
		if !$SpriteRight.visible:
			$SpriteRight.visible = true
			$CollisionPolygonRight.disabled = false
			$SpriteLeft.visible = false
			$CollisionPolygonLeft.disabled = true
			direction_heading = DIRECTION_HEADING.Right
		Thrusting()
		if velocity.x < 0:
			#we are changing direction, so reset accel to max
			accel = MAX_ACCELL
		else:
			accel -= 1
	if Input.is_action_pressed("ui_left"):
		impulse.x -= accel
		if !$SpriteLeft.visible:
			$SpriteLeft.visible = true
			$CollisionPolygonLeft.disabled = false
			$SpriteRight.visible = false
			$CollisionPolygonRight.disabled = true
			direction_heading = DIRECTION_HEADING.Left
		Thrusting()
		if velocity.x > 0:
			#we are changing direction, so reset accel to max
			accel = MAX_ACCELL
		else:
			accel -= 1
	if Input.is_action_pressed("ui_down"):
		
		if velocity.x > 100: #We do this to slow the left/right movement down
			impulse.x += -accel/10
		elif velocity.x < -100:
			impulse.x += accel/10
		impulse.y += accel
		Thrusting()		
		if velocity.y < 0:
			#we are changing direction, so reset accel to max
			accel = MAX_ACCELL
		else:
			accel -= 1
	if Input.is_action_pressed("ui_up"):
		if velocity.x > 100: #We do this to slow the left/right movement down
			impulse.x += -accel/10
		elif velocity.x < -100:
			impulse.x += accel/10
		impulse.y -= accel
		Thrusting()
		if velocity.y > 0:
			#we are changing direction, so reset accel to max
			accel = MAX_ACCELL
		else:
			accel -= 1
	if Input.is_action_pressed("ui_accept"):
		if !player_dying:
			ShootLaser()
		
	if Input.is_action_pressed("ui_hyperspacejump"):
		if !player_dying and !hyperjump_out: #If we're not in a dying/respawn process or already hyperjumping, then jump
			HyperspaceJumpOut()
		
	if Input.is_action_pressed("ui_cancel"):
		$QuitTimer.start()
	
	if Input.is_action_pressed("ui_smartbomb"):
		print("ui_smartbomb")
		if !smart_bomb_active:
			smart_bomb_active = true
			ShootSmartBomb()
		
#	if impulse.length() > 0:
#		print("impulse: " + str(impulse))
		
	#accel = clamp(accel, 0, 5)
	if accel <= 0:
		accel = MAX_ACCELL
		
	if impulse.length() > 0:
		velocity += impulse.normalized() * SPEED
		#print("velocity: " + str(velocity))
		velocity.x = clamp(velocity.x, -550, 480)
		velocity.y = clamp(velocity.y, -200, 200)
		if !$ShipThrust.is_playing():
			$ShipThrust.play()
		
	position += velocity * delta
	
	#print(str(position))

	#We clamp the left and right edges of the map
	position.x = clamp(position.x, $Camera.limit_left + 25, $Camera.limit_right - 25)
	#We clamp the player, taking into account the HUD radar and the lower part of the mountains, which we don't want to go past
	position.y = clamp(position.y, HUD.get_node("Radar").texture.get_size().y + 30, Globals.screensize.y - 20)

func CatchAstronaut(astronaut_body):
	#we are here because the player has caught an astronaut in free-fall
	Globals.AddScore(500)
	astronaut_body.Free()
	$CatchAstronaut.play()
	#print("Set MobAction.ASCENDING")
	astronaut_grabbed = true
	$Astronaut.visible = true
	#$GrabbedAustronautAudio.play()
	StrobePoints500()

func HyperspaceJumpIn():
	$HyperspaceJump.visible = true
	$HyperspaceJump/AnimationPlayer.play_backwards("HyperspaceJump")
	$SpriteLeft.visible = false
	$SpriteRight.visible = false
	
func HyperspaceJumpOut():
	$HyperspaceJump.visible = true
	hyperjump_out = true
	$HyperspaceJump/Audio.play()
	$HyperspaceJump/AnimationPlayer.play("HyperspaceJump")
	$SpriteLeft.visible = false
	$SpriteRight.visible = false

func PlayerDie():
	#print("PlayerDie(): player_dying = " + str(player_dying))
	if !player_dying:
		player_dying = true
		var new_expl = PlayerExplosion.instance()
		Map.add_child(new_expl)
		new_expl.position = self.position
		new_expl.emitting = true
		#print("new_expl.position: " + str(new_expl.position))
		new_expl.connect("player_explosion_finished", self, "_player_explosion_finished")
		self.visible = false
		HUD.DecrementPlayerLives(Globals.player_lives_left)
		Globals.player_lives_left -= 1
		$ExplosionAudio.play()
		$Astronaut.visible = false
		astronaut_grabbed = false

func ReleaseAstronaut():
	Globals.AddScore(500)
	astronaut_grabbed = false
	#astronaut_body.Dropped()
	$Astronaut.visible = false #Hide the astronaut sprite on this node
	$ReleaseAstronaut.play()
	#Spawn a new astronaut node and drop if
	var new_astronaut = NewAstronaut.instance()
	Map.add_child(new_astronaut)
	new_astronaut.position = self.global_position + Vector2(-12,0)
	astronaut_grabbed = false
	#new_astronaut.Dropped() # don't need this as astronaut on the ground already
	StrobePoints500()

func ShootSmartBomb():
	print("ShootSmartBomb()")
	if Globals.smart_bomb_count > 0:
		var smart_bomb = SmartBomb.instance()
		smart_bomb.add_to_group(str(self.get_instance_id()))
		get_parent().add_child(smart_bomb)
		smart_bomb.position = self.global_position
		smart_bomb.connect("smart_bomb_finished", self, "_smart_bomb_finished")
		Globals.smart_bomb_count -= 1
		HUD.UpdateSmartBombs(Globals.smart_bomb_count)

func ShootLaser():
	#Only shoot the laser if we can fire, based on firing rate
	if can_fire:
		
		var laser_beam = LaserBeam.instance()
		
		laser_beam.add_to_group(str(self.get_instance_id()))
		#print("add_to_group(): " + str(self.get_instance_id()))
		get_parent().add_child(laser_beam)
		if $SpriteRight.visible:
			laser_beam.position = $SpriteRight/MuzzleRight.global_position
			laser_beam.speed_x = 2
		else:
			laser_beam.position = $SpriteLeft/MuzzleLeft.global_position - Vector2(laser_beam.get_node("Sprite").texture.get_size().x - laser_beam.get_node("Sprite").position.x - 25 ,0)
			laser_beam.speed_x = -2
		can_fire = false
		$FiringTimer.start()
		$Laser.play()

func SpawnPlayer():
	if Globals.player_lives_left <= 0:
		#Game over man, game over!
		game_over = true
		$SpriteLeft.visible = false
		$SpriteRight.visible = false
		HUD.ShowMessage("Game Over")
		$GameOverTimer.start()
		Globals.player_lives_left = 5
	else:
		$SpawnAudio.play()
		self.global_position = Vector2(int(rand_range(100, 8000)), int(rand_range(50, 450)))
		self.visible = true
		var curr_sprite
		respawn_countdown -= 1
		if $SpriteLeft.visible == true:
			curr_sprite = $SpriteLeft
		else:
			curr_sprite = $SpriteRight
		$Tween.interpolate_property(curr_sprite, "modulate" , Color(1, 1, 1, 0.2), Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		$SpawnTweenTimer.start()

func StrobePoints500():
	$Points500.Strobe()
	
func Thrusting():
	if $SpriteRight.visible:
		$SpriteRight/ThurstRight1.emitting = true
		$SpriteRight/ThurstRight2.emitting = true
	else:
		$SpriteLeft/ThurstLeft1.emitting = true
		$SpriteLeft/ThurstLeft2.emitting = true

	$ThrustTimer.start()
	

func _hyperspace_jump_finished():
	#$HyperspaceJump.visible = false
	if !hyperjump_out:
		#jump to new pos
		if direction_heading == DIRECTION_HEADING.Right:
			$SpriteRight.visible = true
		else:
			$SpriteLeft.visible = true
	else:
		self.global_position = Vector2(int(rand_range(100, 8000)), int(rand_range(50, 450)))
		print("HyperjumpPlayerOut() new_pos: " + str(self.global_position))
		hyperjump_out = false 
		HyperspaceJumpIn()

func _smart_bomb_finished():
	smart_bomb_active = false
	
func _player_explosion_finished():
	SpawnPlayer()

func _on_ThurstTimer_timeout():
	$SpriteRight/ThurstRight1.emitting = false
	$SpriteRight/ThurstRight2.emitting = false
	$SpriteLeft/ThurstLeft1.emitting = false
	$SpriteLeft/ThurstLeft2.emitting = false
	$ShipThrust.stop()


func _on_FiringTimer_timeout():
	can_fire = true


func _on_Player_area_entered(area):
	#print("Player hit by: " + str(area))
	if area.get_groups().has("bomb") and area.visible: #Only get hit if mob is visible, else, mob dying:
		PlayerDie()
	elif area.get_groups().has("mob") and area.visible: #Only get hit if mob is visible, else, mob dying
		PlayerDie()
	elif area.get_groups().has("astronaut") and area.dropped: #Only grab this astronaut if it is dropped/falling.
		CatchAstronaut(area)
	elif astronaut_grabbed and area.get_groups().has("mountains"): #Only drop the astronaut we've collected if we're carrying one and we pass through a mountain
		ReleaseAstronaut()
	
	

func _on_SpawnTweenTimer_timeout():
	if respawn_countdown > 0:
		var curr_sprite
		respawn_countdown -= 1
		if $SpriteLeft.visible == true:
			curr_sprite = $SpriteLeft
		else:
			curr_sprite = $SpriteRight
		if spawn_phase_in:
			$Tween.interpolate_property(curr_sprite, "modulate" , Color(1, 1, 1, 0.2), Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
			spawn_phase_in = false
			#print("_on_SpawnTweenTimer_timeout(): phasing in")
		else:
			$Tween.interpolate_property(curr_sprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.2), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
			#print("_on_SpawnTweenTimer_timeout(): phasing out")
			spawn_phase_in = true
		$Tween.start()
	else:
		$SpawnTweenTimer.stop()
		$Tween.interpolate_property($SpriteLeft, "modulate" , Color(1, 1, 1, 0.2), Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.interpolate_property($SpriteRight, "modulate" , Color(1, 1, 1, 0.2), Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
		player_dying = false
		respawn_countdown = RESPAWN_MAX_COUNT



func _on_Player_body_entered(body):
	if body.get_groups().has("mob") and body.visible == true: #Only get hit if mob is visible, else, mob dying
		PlayerDie()


func _on_GameOverTimer_timeout():
	Globals.GameOver()
	


func _on_QuitTimer_timeout():
	Globals.GameOver()
