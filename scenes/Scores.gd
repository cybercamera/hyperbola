extends Node


var player setget player_set, player_get
var score setget score_set, score_get

func _init(player_name, score_num):
	name = player_name
	self.player = player_name
	self.score = score_num


func player_set(new_player):
    player = new_player

func player_get():
    return player # Getter must return a value.

func score_set(new_score):
    score = new_score

func score_get():
    return score # Getter must return a value.
