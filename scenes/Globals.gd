extends Node

#const ScoresClass = preload("res://scenes/Scores.gd")
onready var HUD # = get_tree().get_root().get_node("Map").get_node("HUD")

const PLAYER_TERRAIN_WIDTH = 8800 #This is the default width of the mountains sprite
const HUD_TERRAIN_WIDTH = 1170

const NEW_LIFE_1 = 30000
const NEW_LIFE_2 = 100000
const NEW_LIFE_3 = 250000
const NEW_LIFE_4 = 500000
const NEW_LIFE_5 = 1000000

#const NEW_LIFE_1 = 300
#const NEW_LIFE_2 = 1000
#const NEW_LIFE_3 = 1500
#const NEW_LIFE_4 = 2000
#const NEW_LIFE_5 = 2500



const TOP_SCORES_FILE = "res://top_scores.json"
const MAX_PLAYER_LIVES = 5

var player_lives_left = MAX_PLAYER_LIVES
var new_life_achieved = { "NEW_LIFE_1": false,  "NEW_LIFE_2": false, "NEW_LIFE_3": false, "NEW_LIFE_4": false, "NEW_LIFE_5": false}
var player_lives_counter = 0


var terrain_scaling = 0.0 #The ration between the full map mountains and the radar mountains widths
var vertical_scaling = 0.1 #The ration between the full map and radar heights

var screensize = Vector2()
var current_score = 0
var blip_array = {}
var top_scores = {}
var player_name
var paused = false

var smart_bomb_count = 3
var smart_bomb_counter = 0

#func _ready():
#	print(str(HUD))

func InitiateTree():
	HUD = get_tree().get_root().get_node("Map").get_node("HUD")
	
func AddScore(points):
	current_score += points
	HUD.ShowScore(current_score)
#	if current_score >= NEW_LIFE_5 and !new_life_achieved["NEW_LIFE_5"]: #We countdown from 5, to prevent a fizzbuzz issue. Also, we don't give the player this life again if they've already achieved.
#		new_life_achieved["NEW_LIFE_5"] = true
#		player_lives_left += 1
#		HUD.IncrementPlayerLives(player_lives_left)
#	elif current_score >= NEW_LIFE_4 and !new_life_achieved["NEW_LIFE_4"]: 
#		new_life_achieved["NEW_LIFE_4"] = true
#		player_lives_left += 1
#		HUD.IncrementPlayerLives(player_lives_left)
#	elif current_score >= NEW_LIFE_3 and !new_life_achieved["NEW_LIFE_3"]: 
#		new_life_achieved["NEW_LIFE_3"] = true
#		player_lives_left += 1
#		HUD.IncrementPlayerLives(player_lives_left)
#	elif current_score >= NEW_LIFE_2 and !new_life_achieved["NEW_LIFE_2"]: 
#		new_life_achieved["NEW_LIFE_2"] = true
#		player_lives_left += 1
#		HUD.IncrementPlayerLives(player_lives_left)
#	elif current_score >= NEW_LIFE_1 and !new_life_achieved["NEW_LIFE_1"]: 
#		new_life_achieved["NEW_LIFE_1"] = true
#		player_lives_left += 1
#		HUD.IncrementPlayerLives(player_lives_left)
	
	player_lives_counter += points
	#Calculate if we're getting a new player life
	
	if player_lives_counter >= 10000: #If the counter says we've hit over 5000, give another smart bomb
		player_lives_left += 1 
		HUD.IncrementPlayerLives(player_lives_left)
		player_lives_counter = 0 #Reset the counter
	
	smart_bomb_counter += points
	#Calculate if we're getting a new smartbomb
	
	if smart_bomb_counter >= 5000: #If the counter says we've hit over 5000, give another smart bomb
		smart_bomb_count += 1 
		HUD.UpdateSmartBombs(smart_bomb_count)
		smart_bomb_counter = 0 #Reset the counter
		
	
func GameOver():
	#print("GameOver()")
	Globals.SaveCurrentScore()
	Globals.SaveScores()
	get_tree().get_root().get_node("Map").queue_free()
	get_tree().change_scene("res://scenes/MainMenu.tscn")
	
	
func SaveCurrentScore():
	#This is called when the the game is over so that the player's score can be added to the great list.
	top_scores[top_scores.size()] = [player_name, current_score] 
	
func LoadScores():
	#print("LoadScores()")
	var file = File.new()
	if not file.file_exists(TOP_SCORES_FILE):
	    print("No file saved!")
	    return
	
	# Open existing file
	if file.open(TOP_SCORES_FILE, File.READ) != 0:
	    print("Error opening file")
	    return
	
	# Get the data
	var data = {}
	data = parse_json(file.get_line())
	
	var top_scores_text = "\t\t\t\t\tHall of Fame\n"
	if !data:
		return
	#var json = parse_json(file.get_line())
	# Then do what you want with the data
	for i in range(data.size()):
		top_scores[data.keys()[i]] = data.values()[i]
		top_scores_text += str("%04d" % int(data.keys()[i])) + ":\t " +  str("%010d" % data.values()[i][1]) + "\t\t" + str(data.values()[i][0]) + "\n"
	
	return top_scores_text
	
func SaveScores():
	#print("SaveScores()")
	# Open a file
	var file = File.new()
	if file.open(TOP_SCORES_FILE, File.WRITE) != 0:
	    print("Error opening file")
	    return
	
	var sorted_scores = Array()

	#We have to find if the current score is bigger than any existing score. If so, add it 
	for i in range(top_scores.size()):
		print("Saving scores: " + str(top_scores.values()[i][0]) + ": " +  str(top_scores.values()[i][1]))
		sorted_scores.append([top_scores.values()[i][0], top_scores.values()[i][1]])
	
	#print(str(sorted_scores))
#	print(str(sorted_names))
	#Sort the array, based on score
	sorted_scores.sort_custom(self, "custom_array_sort") #Don't know if this works!!
	#print(str(sorted_scores))
	
	#We need to re-write the top_scores
	top_scores.clear()
	#write the dictionary out as {0: ["conz", 1900], 1: ["eliana", 1950]]
	#We only want the top 50 scores. So, if more, we cap it:
	var top_score_num = sorted_scores.size() 
	if top_score_num > 50:
		 top_score_num = 50
	for i in range(top_score_num):
		top_scores[i] = [sorted_scores[i][0], sorted_scores[i][1]] 
	
	# Save the dictionary as JSON
	file.store_line(to_json(top_scores))
	file.close()
	
func custom_array_sort(a, b):
	# Check if the roll of a is less than the roll of b.
	return a[1] > b[1]