extends Particles2D

func _ready():
	$AnimationPlayer.play("Embers")

func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()
