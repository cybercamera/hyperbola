extends Area2D

onready var Map = get_parent().get_parent().get_parent()
onready var HUD_Mountains = get_tree().get_root().get_node("Map").get_node("HUD").get_node("Mountains")
onready var MobPathFollow = get_parent() #This should be the SaucerPathFollow

onready var MobExplosion = preload("res://scenes/MobExplosion.tscn")
onready var MobBomb = preload("res://scenes/SquareBomb.tscn")
onready var RadarBlip = preload("res://scenes/SquareRadarBlip.tscn")

export var active = false #Use this toggle to allow this node to be self-propelled, rather than relying on the Follow2D
export var speed = 300

const SCORE = 200

var direction = Vector2()
var velocity = Vector2()
var astronaut_pos = Vector2()
var mob_dying = false
var mob_hit = false
var hyperjump_out = false
var player_target_area
var can_drop_bomb = false
var my_radar_blip

func _ready():
	randomize()
	$HyperjumpOutTimer.wait_time = randi() % 15 + 5
	add_to_group("mob")
	add_to_group("square")
	#Hide the sprite until hyperspace jump complete
	$Sprite1.visible = false
	$Sprite2.visible = false 
	$Sprite3.visible = false 
	$Sprite4.visible = false 
	$HyperspaceJump.connect("hyperspace", self, "_hyperspace_jump_finished")
	HyperjumpIn()
	SpawnRadarBlip()
	

func _process(delta):
	if active:
		#we are here because we're self propelled. 
		move_and_slide(direction.normalized() * speed)
		
	
	if can_drop_bomb:
		#if it's time that we can shoot a bomb, let's shoot one.
		DropBomb()
		can_drop_bomb = false
	
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600

func DropBomb():
	#Squares (bombers) don't shoot bombs, they drop them.
	var bomb = MobBomb.instance()
	bomb.global_position = self.global_position
	bomb.add_to_group("bomb")
	#print("add_to_group(): " + str(self.get_instance_id()))
	Map.add_child(bomb)

func Free():
	Map.DecrementMobCount()
	my_radar_blip.queue_free() #Get rid of the radar blip
	get_parent().get_parent().queue_free() #we free the lander path

func HyperjumpIn():
	$HyperspaceJump.visible = true
	$HyperspaceJump/AnimationPlayer.play_backwards("HyperspaceJump")
	
func HyperjumpOut():
	$HyperspaceJump.visible = true
	hyperjump_out = true
	$HyperspaceJump/AnimationPlayer.play("HyperspaceJump")
	$Sprite1.visible = false
	$Sprite2.visible = false
	$Sprite3.visible = false
	$Sprite4.visible = false


func MobDie():
	if !mob_dying:
		#Globals.blip_array[str(self)] = null #Globals.blip_array.erase()
		mob_dying = true
		var new_expl = MobExplosion.instance()
		Map.add_child(new_expl)
		new_expl.position = self.global_position
		new_expl.emitting = true
		#print("new_expl.position: " + str(new_expl.position))
		self.visible = false
		Globals.AddScore(SCORE)
		$ExplosionAudio.play()


func SpawnRadarBlip():
	my_radar_blip = RadarBlip.instance()
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	#print("SpawnRadarBlip() x,y: " + str(my_radar_blip.position.x) + ", " + str(my_radar_blip.position.y))
	HUD_Mountains.add_child(my_radar_blip)

func _hyperspace_jump_finished():
	#Let's start the cycletimer so that the lander becomes visible
	$CycleTimer.start()
	#$HyperspaceJump.visible = false
	if hyperjump_out:
		#Hide the hyperspacejump
		$HyperspaceJump.visible = false
		#jump to new pos
		MobPathFollow.unit_offset = randf()
		hyperjump_out = false
		HyperjumpIn()

func _on_CycleTimer_timeout():
	if $Sprite1.visible:
		$Sprite1.visible = false
		$Sprite2.visible = true
		$Sprite3.visible = false
		$Sprite4.visible = false
	elif $Sprite2.visible:
		$Sprite1.visible = false
		$Sprite2.visible = false
		$Sprite3.visible = true
		$Sprite4.visible = false
	elif $Sprite3.visible:
		$Sprite1.visible = false
		$Sprite2.visible = false
		$Sprite3.visible = false
		$Sprite4.visible = true
	else:
		$Sprite1.visible = true
		$Sprite2.visible = false
		$Sprite3.visible = false
		$Sprite4.visible = false

func _on_ExplosionAudio_finished():
	Free()


func _on_DropBombTimer_timeout():
	can_drop_bomb = true


func _on_PlayerScanner_area_entered(area):
	#print("_on_PlayerScanner_area_entered(area): " + str(area.get_name()))
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) player spotted")
		$DropBombTimer.start()
		player_target_area = area

func _on_PlayerScanner_area_exited(area):
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) left")
		#Player exited the scanner area, so stop targeting them
		$DropBombTimer.stop()


func _on_Square_area_entered(area):
	#print("Saucer area entered: " + str(area.get_name()))
	if area.get_groups().has("laser") and !mob_hit:
		#print("Lander hit by laser") 
		mob_hit = true #We shouldn't be shot again
		#we've been shot!
		MobDie()


func _on_HyperjumpOutTimer_timeout():
	$HyperjumpOutTimer.wait_time = randi() % 15 + 5
	HyperjumpOut()
