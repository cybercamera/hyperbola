extends Path2D


onready var Map = get_parent()
export var speed = 120

var free_path = false

func _ready():
	add_to_group("mob_path")
	randomize()
	$Follow.unit_offset = randf() #we set a random point of location of this mob on the follow path, between 0 & 1

func _process(delta):
	$Follow.set_offset($Follow.get_offset() + speed * delta)
	
func Free():
	$Follow/Pod.Free()
	
func GetMobPosition():
	#We need to find the exact position of the mob held by this path
	return $Follow/Pod.global_position