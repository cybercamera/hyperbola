extends Area2D

signal smart_bomb_finished

func _ready():
	print("smartbomb blows!")
	add_to_group("laser")
	$Tween.interpolate_property($Shockwave, "scale" , $Shockwave.scale, $Shockwave.scale * 10 ,0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Light2D, "scale" , $Light2D.scale, $Light2D.scale * 10 ,0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Sprite, "scale" , $Sprite.scale, $Sprite.scale * 10 ,0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	


func _on_Tween_tween_completed(object, key):
	print("smart_bomb_finished")
	emit_signal("smart_bomb_finished")
	queue_free()
