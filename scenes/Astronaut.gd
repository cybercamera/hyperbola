extends Area2D

onready var Map = get_tree().get_root().get_node("Map")
onready var HUD_Mountains = get_tree().get_root().get_node("Map").get_node("HUD").get_node("Mountains")
onready var AstronautExplosion = preload("res://scenes/AstronautExplosion.tscn")
onready var RadarBlip = preload("res://scenes/AstronautRadarBlip.tscn")

const SAFE_HEIGHT = 150

export var dropped = false

var grav = Vector2(0, 0)  # gravity force
var velocity = Vector2()  # the area's velocity
var dropped_height #height at which astronaut is dropped. 
var astronaut_hit = false
var my_radar_blip



func _ready():
	add_to_group("astronaut")
	SpawnRadarBlip()

func _process(delta):
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	if dropped:
	    velocity += grav * delta 
	    position += velocity * delta

func AstronautDies():
	if !astronaut_hit:
		astronaut_hit = true
		#Globals.blip_array[str(self)] = null #Globals.blip_array.erase()
		$AstronautExplosion.play()
		var new_expl = AstronautExplosion.instance()
		Map.add_child(new_expl)
		new_expl.position = self.global_position
		new_expl.emitting = true
		#print("new_expl.position: " + str(new_expl.position))
		self.visible = false
		

func Dropped():
	grav = Vector2(0,100)
	dropped = true
	#scale = Vector2(10,10)
	dropped_height = position.y
	#print("dropped_height: " + str(dropped_height))

func Free():
	Map.DecrementAstronautCount()
	my_radar_blip.queue_free() #Get rid of the radar blip
	queue_free()

func SpawnRadarBlip():
	my_radar_blip = RadarBlip.instance()
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	#print("SpawnRadarBlip() x,y: " + str(my_radar_blip.position.x) + ", " + str(my_radar_blip.position.y))
	HUD_Mountains.add_child(my_radar_blip)


func _on_Astronaut_area_entered(area):
	if area.get_groups().has("mountains"):
		velocity = Vector2(0,0)
		grav = Vector2(0,0)
		#If the distance from the point at which were dropped to the current height is high enough, we die.
		if dropped:
			var drop_dist 
			drop_dist = abs(dropped_height - position.y)
			#print("drop_dist: %s dropped_height: %s position.y: %s" % [drop_dist, dropped_height, position.y])
			dropped = false
			if drop_dist > SAFE_HEIGHT:
				AstronautDies()
	
	if area.get_groups().has("laser"):
		#astronaut has been hit by lasers
		#rint("astronaut hit by laser!")
		AstronautDies()


func _on_Timer_timeout():
	if dropped:
		#print ("dropped astronaut pos: " + str(position))
		pass


func _on_AstronautExplosion_finished():
	Free()
