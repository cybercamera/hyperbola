extends Area2D

onready var Map = get_parent().get_parent().get_parent()
onready var HUD_Mountains = get_tree().get_root().get_node("Map").get_node("HUD").get_node("Mountains")

onready var MobExplosion = preload("res://scenes/MobExplosion.tscn")
onready var MutantBomb = preload("res://scenes/MutantBomb.tscn")
onready var RadarBlip = preload("res://scenes/MutantRadarBlip.tscn")

export var active = false #Use this toggle to allow this node to be self-propelled, rather than relying on the Follow2D
export var speed = 100

const SCORE = 150

var direction = Vector2()
var velocity = Vector2()
var astronaut_pos = Vector2()
var mob_dying = false
var mob_hit = false
var hyperjump_out = false
var player_target_area
var can_shoot_bomb = false
var my_radar_blip

func _ready():
	add_to_group("mob")
	add_to_group("mutant")
	$SpriteRight.visible = true #Hide the sprite until hyperspace jump complete
	$SpriteLeft.visible = false
	SpawnRadarBlip()
	

func _process(delta):
	if active:
		#we are here because we're self propelled. 
		move_and_slide(direction.normalized() * speed)
		
	
	if can_shoot_bomb:
		#if it's time that we can shoot a bomb, let's shoot one.
		ShootBomb(player_target_area.global_position)
		can_shoot_bomb = false
	
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600

func Free():
	Map.DecrementMobCount()
	my_radar_blip.queue_free() #Get rid of the radar blip
	get_parent().get_parent().queue_free()

func MobDie():
	if !mob_dying:
		#Globals.blip_array[str(self)] = null #Globals.blip_array.erase()
		mob_dying = true
		var new_expl = MobExplosion.instance()
		Map.add_child(new_expl)
		new_expl.position = self.global_position
		new_expl.emitting = true
		#print("new_expl.position: " + str(new_expl.position))
		self.visible = false
		Globals.AddScore(SCORE)
		$ExplosionAudio.play()

func ShootBomb(target_pos):
	var target_dir = target_pos - self.global_position
	target_dir = target_dir.normalized()
	var bomb = MutantBomb.instance()
	bomb.global_position = self.global_position
	bomb.speed_x = target_dir.x 
	bomb.speed_y = target_dir.y
	bomb.add_to_group("bomb")
	#print("add_to_group(): " + str(self.get_instance_id()))
	Map.add_child(bomb)

func SpawnRadarBlip():
	my_radar_blip = RadarBlip.instance()
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	#print("SpawnRadarBlip() x,y: " + str(my_radar_blip.position.x) + ", " + str(my_radar_blip.position.y))
	HUD_Mountains.add_child(my_radar_blip)

func _on_CycleTimer_timeout():
	if $SpriteLeft.visible:
		$SpriteLeft.visible = false
		$SpriteRight.visible = true
	else:
		$SpriteLeft.visible = true
		$SpriteRight.visible = false


func _on_Mutant_area_entered(area):
	#print("Mutant area entered: " + str(area.get_name()))
	if area.get_groups().has("laser") and !mob_hit:
		#print("Lander hit by laser") 
		mob_hit = true #We shouldn't be shot again
		#we've been shot!
		MobDie()

func _on_ExplosionAudio_finished():
	Free()


func _on_ShootBombTimer_timeout():
	can_shoot_bomb = true


func _on_PlayerScanner_area_entered(area):
	#print("_on_PlayerScanner_area_entered(area): " + str(area.get_name()))
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) player spotted")
		$ShootBombTimer.start()
		player_target_area = area

func _on_PlayerScanner_area_exited(area):
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) left")
		#Player exited the scanner area, so stop targeting them
		$ShootBombTimer.stop()
