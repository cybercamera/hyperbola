extends Area2D

onready var Map = get_parent()
onready var HUD_Mountains = get_tree().get_root().get_node("Map").get_node("HUD").get_node("Mountains")

onready var MobExplosion = preload("res://scenes/MobExplosion.tscn")
onready var MobBomb = preload("res://scenes/SwarmerBomb.tscn")
onready var RadarBlip = preload("res://scenes/SwarmerRadarBlip.tscn")

export var speed = 250
export var speed_x = 1
export var speed_y = 0

const SCORE = 150

var direction = Vector2()
var velocity = Vector2()
var astronaut_pos = Vector2()
var mob_dying = false
var mob_hit = false
var hyperjump_out = false
var player_target_area
var can_shoot_bomb = false
var my_radar_blip

var old_modulation
var phase_in = true

func _ready():
	add_to_group("mob")
	add_to_group("swarmer")
	old_modulation =  $Sprite.modulate
	$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.4), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	$PlayerFinderTimer.wait_time = randf() + 0.3 #We do this so that the swarmers in a swarm don't all find the player at the same time
	$ShootBombTimer.wait_time = randf() + 0.4 #We do this so that the swarmers in a swarm don't all shoot at the same time
	SpawnRadarBlip()
	

func _process(delta):
	if direction.length() == 0:
		#We are here because we'e not spotted the player as yet, so we are given impulse from the pod
		var motion = Vector2(speed_x, speed_y) * speed
		self.position = self.position + motion * delta
	else:
		#The player has been spotted, so give chase !
		var motion = direction.normalized() * speed
		self.position = self.position + motion * delta
	
		position.x = clamp(position.x, 300, Globals.PLAYER_TERRAIN_WIDTH - 300)
		position.y = clamp(position.y, 120, 550)	
	
	if can_shoot_bomb:
		#if it's time that we can shoot a bomb, let's shoot one.
		ShootBomb(player_target_area.global_position)
		can_shoot_bomb = false
	
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600

func Free():
	Map.DecrementMobCount()
	my_radar_blip.queue_free() #Get rid of the radar blip
	queue_free()

func MobDie():
	if !mob_dying:
		#Globals.blip_array[str(self)] = null #Globals.blip_array.erase()
		mob_dying = true
		var new_expl = MobExplosion.instance()
		Map.add_child(new_expl)
		new_expl.position = self.global_position
		new_expl.emitting = true
		#print("new_expl.position: " + str(new_expl.position))
		self.visible = false
		Globals.AddScore(SCORE)
		$ExplosionAudio.play()

func ShootBomb(target_pos):
	var target_dir = target_pos - self.global_position
	target_dir = target_dir.normalized()
	var bomb = MobBomb.instance()
	bomb.global_position = self.global_position
	bomb.speed_x = target_dir.x 
	bomb.speed_y = target_dir.y
	bomb.add_to_group("bomb")
	#print("add_to_group(): " + str(self.get_instance_id()))
	Map.add_child(bomb)

func SpawnRadarBlip():
	my_radar_blip = RadarBlip.instance()
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	#print("SpawnRadarBlip() x,y: " + str(my_radar_blip.position.x) + ", " + str(my_radar_blip.position.y))
	HUD_Mountains.add_child(my_radar_blip)



func _on_Swarmer_area_entered(area):
	#print("Mutant area entered: " + str(area.get_name()))
	if area.get_groups().has("laser") and !mob_hit:
		print("Swarmer hit by laser") 
		mob_hit = true #We shouldn't be shot again
		#we've been shot!
		MobDie()

func _on_ExplosionAudio_finished():
	Free()


func _on_ShootBombTimer_timeout():
	can_shoot_bomb = true


func _on_PlayerScanner_area_entered(area):
	#print("_on_PlayerScanner_area_entered(area): " + str(area.get_name()))
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) player spotted")
		$ShootBombTimer.start()
		$PlayerFinderTimer.start()
		player_target_area = area

func _on_PlayerScanner_area_exited(area):
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) left")
		#Player exited the scanner area, so stop targeting them
		$ShootBombTimer.stop()
		$PlayerFinderTimer.stop()


func _on_Tween_tween_completed(object, key):
	if phase_in:
		$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 0.4), Color(1, 1, 1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		phase_in = false
	else:
		$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.4), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		phase_in = true
	$Tween.start()


func _on_PlayerFinderTimer_timeout():
	direction =  player_target_area.global_position - self.global_position
