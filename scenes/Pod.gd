extends Area2D

onready var Map = get_parent().get_parent().get_parent()
onready var HUD_Mountains = get_tree().get_root().get_node("Map").get_node("HUD").get_node("Mountains")
onready var MobPathFollow = get_parent() #This should be the SaucerPathFollow

onready var MobExplosion = preload("res://scenes/MobExplosion.tscn")
onready var MobBomb = preload("res://scenes/PodBomb.tscn")
onready var RadarBlip = preload("res://scenes/PodRadarBlip.tscn")
onready var NewSwarmer = preload("res://scenes/Swarmer.tscn")

export var active = false #Use this toggle to allow this node to be self-propelled, rather than relying on the Follow2D
export var speed = 100

const SCORE = 1000

var direction = Vector2()
var velocity = Vector2()
var mob_dying = false
var mob_hit = false
var hyperjump_out = false
var player_target_area
var can_shoot_bomb = false
var my_radar_blip

var old_modulation
var phase_in = true

func _ready():
	randomize()
	$HyperjumpOutTimer.wait_time = randi() % 25 + 5
	add_to_group("mob")
	add_to_group("pod")
	#Hide the sprite until hyperspace jump complete
	$Sprite.visible = false
	old_modulation =  $Sprite.modulate
	$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.4), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	$HyperspaceJump.connect("hyperspace", self, "_hyperspace_jump_finished")
	HyperjumpIn()
	SpawnRadarBlip()
	

func _process(delta):
	if can_shoot_bomb:
		#if it's time that we can shoot a bomb, let's shoot one.
		ShootBomb(player_target_area.global_position)
		can_shoot_bomb = false
	
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600

func Free():
	Map.DecrementMobCount()
	my_radar_blip.queue_free() #Get rid of the radar blip
	get_parent().get_parent().queue_free() #we free the lander path

func HyperjumpIn():
	$HyperspaceJump.visible = true
	$HyperspaceJump/AnimationPlayer.play_backwards("HyperspaceJump")
	
func HyperjumpOut():
	$HyperspaceJump.visible = true
	hyperjump_out = true
	$HyperspaceJump/AnimationPlayer.play("HyperspaceJump")
	$Sprite.visible = false


func MobDie():
	if !mob_dying:
		#Globals.blip_array[str(self)] = null #Globals.blip_array.erase()
		mob_dying = true
		var new_expl = MobExplosion.instance()
		Map.add_child(new_expl)
		new_expl.position = self.global_position
		new_expl.emitting = true
		#print("new_expl.position: " + str(new_expl.position))
		self.visible = false
		Globals.AddScore(SCORE)
		$ExplosionAudio.play()
		

func ShootBomb(target_pos):
	var target_dir = target_pos - self.global_position
	target_dir = target_dir.normalized()
	var bomb = MobBomb.instance()
	bomb.global_position = self.global_position
	bomb.speed_x = target_dir.x 
	bomb.speed_y = target_dir.y
	bomb.add_to_group("bomb")
	#print("add_to_group(): " + str(self.get_instance_id()))
	Map.add_child(bomb)

func SpawnRadarBlip():
	my_radar_blip = RadarBlip.instance()
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	#print("SpawnRadarBlip() x,y: " + str(my_radar_blip.position.x) + ", " + str(my_radar_blip.position.y))
	HUD_Mountains.add_child(my_radar_blip)

func SpawnSwarmers():
	#spawn a random number of swarmers 
	for i in range(randi() % 2 + 2): #upto 4 swarmers
		var new_swarmer = NewSwarmer.instance()
		new_swarmer.global_position = self.global_position + Vector2(rand_range(-20,20), rand_range(-20,20))
		#print("new_swarmer.global_position: " + str(new_swarmer.global_position) + " pod.global_position: " + str(self.global_position))
		Map.add_child(new_swarmer)
		Map.IncrementMobCount() #Make sure we click the counter up for each swarmer we make
		new_swarmer.speed_x = rand_range(-1,1) 
		new_swarmer.speed_y = rand_range(0,0)
		#print("new_swarmer: " + str(new_swarmer.global_position))

func _hyperspace_jump_finished():
	#Let's start the cycletimer so that the lander becomes visible
	$Sprite.visible = true
	#$HyperspaceJump.visible = false
	if hyperjump_out:
		#Hide the hyperspacejump
		$HyperspaceJump.visible = false
		#jump to new pos
		MobPathFollow.unit_offset = randf()
		hyperjump_out = false
		HyperjumpIn()

func _on_CycleTimer_timeout():
	if $SpriteLeft.visible:
		$SpriteLeft.visible = false
		$SpriteMiddle.visible = true
		$SpriteRight.visible = false
	elif $SpriteMiddle.visible:
		$SpriteLeft.visible = false
		$SpriteMiddle.visible = false
		$SpriteRight.visible = true
	else:
		$SpriteLeft.visible = true
		$SpriteMiddle.visible = false
		$SpriteRight.visible = false


func _on_ExplosionAudio_finished():
	SpawnSwarmers()
	Free()


func _on_ShootBombTimer_timeout():
	can_shoot_bomb = true


func _on_PlayerScanner_area_entered(area):
	#print("_on_PlayerScanner_area_entered(area): " + str(area.get_name()))
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) player spotted")
		$ShootBombTimer.start()
		player_target_area = area

func _on_PlayerScanner_area_exited(area):
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) left")
		#Player exited the scanner area, so stop targeting them
		$ShootBombTimer.stop()


func _on_Saucer_area_entered(area):
	#print("Saucer area entered: " + str(area.get_name()))
	if area.get_groups().has("laser") and !mob_hit:
		#print("Lander hit by laser") 
		mob_hit = true #We shouldn't be shot again
		#we've been shot!
		MobDie()


func _on_HyperjumpOutTimer_timeout():
	$HyperjumpOutTimer.wait_time = randi() % 15 + 5
	HyperjumpOut()


func _on_Tween_tween_completed(object, key):
	if phase_in:
		$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 0.4), Color(1, 1, 1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		phase_in = false
	else:
		$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.4), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		phase_in = true
	$Tween.start()
