extends Node2D

onready var LanderPath = preload("res://scenes/LanderPath1.tscn")
onready var SaucerPath = preload("res://scenes/SaucerPath1.tscn")
onready var SquarePath = preload("res://scenes/SquarePath1.tscn")
onready var MutantPath = preload("res://scenes/MutantPath2.tscn")
onready var PodPath = preload("res://scenes/PodPath1.tscn")

onready var NewAstronaut = preload("res://scenes/Astronaut.tscn")

export var init_lander_spawn = 5
export var init_saucer_spawn = 2
export var init_square_spawn = 2
export var init_pod_spawn = 1

export var init_astronaut_spawn = 30


var num_landers_launched = 0
var num_saucers_launched = 0
var num_squares_launched = 0
var num_pods_launched = 0
var total_mob_count = 0 
var num_astronauts_count = 0

var max_landers = 15 #Once we launch this many, then there are no more for this round
var max_saucers = 6
var max_squares = 8
var max_pods = 4

var cur_wave = 1
var planet_disintegrated = false

var LanderSpawnPoints = Array()
var SaucerSpawnPoints = Array()
var SquareSpawnPoints = Array()
var PodSpawnPoints = Array()
var PlanetExplosionPoints = Array()

func _ready():
	Globals.InitiateTree()
	Globals.screensize = get_viewport_rect().size
	GetLanderSpawnPoints()
	GetSaucerSpawnPoints()
	GetSquareSpawnPoints()
	GetPodSpawnPoints()
	GetPlanetExplosionPoints()
	$PlanetDisintegrateEffect.interpolate_property($Mountains, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.0), 1.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	SpawnInitMobs()
	SpawnInitAstronauts()

func CalculateBonus():
	return num_astronauts_count * 100

func ConvertAllMobsToMutants():
	print("ConvertAllMobsToMutants()")
	for node in get_children():
		print("node: " + node.get_name())
		if node.get_groups().has("mob_path"): 
			#we spawn a new mutant for each mob, using the old mob's exact position
			var mut_pos = node.GetMobPosition()
			SpawnMutant(mut_pos)
			#we get rid of each mob
			node.Free()
	

func DecrementMobCount():
	#This is called every time a mob dies, so that when the total hits zero, the wave ends
	total_mob_count -= 1
	print("total_mob_count: " + str(total_mob_count))
	if total_mob_count <= 0 and num_landers_launched == max_landers and num_saucers_launched == max_saucers:
		#The wave has ended 
		NextWave()
	
func DecrementAstronautCount():
	#This is called every time a mob dies, so that when the total hits zero, the wave ends
	num_astronauts_count -= 1
	print("num_astronauts_count: " + str(num_astronauts_count))
	if num_astronauts_count <= 0:
		#The wave has ended 
		DisintegratePlanet()
		
func DisintegratePlanet():
	print("DisintegratePlanet()")
	if !planet_disintegrated:
		planet_disintegrated = true
		$PlanetExplosionAudio.play()
		$PlanetDisintegrateEffect.start()
		$HUD/Mountains/MountainsSprite.visible = false
		ConvertAllMobsToMutants()
		for expl in PlanetExplosionPoints:
			#Let's iterate our way through the whole list and tell it to explode. It can figure out if it is visible and this needs to
			expl.Explode()
	
func GetLanderSpawnPoints():
	for node in $MobSpawnPoints.get_children():
		var node_name = node.get_name()
		if node_name.begins_with("LanderSpawn"): 
			LanderSpawnPoints.append(node)

func GetPlanetExplosionPoints():
	for node in $PlanetExplosionPoints.get_children():
		var node_name = node.get_name()
		if node_name.begins_with("PlanetExplosion"): 
			PlanetExplosionPoints.append(node)

func GetPodSpawnPoints():
	for node in $MobSpawnPoints.get_children():
		var node_name = node.get_name()
		if node_name.begins_with("PodSpawn"): 
			PodSpawnPoints.append(node)

func GetSaucerSpawnPoints():
	for node in $MobSpawnPoints.get_children():
		var node_name = node.get_name()
		if node_name.begins_with("SaucerSpawn"): 
			SaucerSpawnPoints.append(node)

func GetSquareSpawnPoints():
	for node in $MobSpawnPoints.get_children():
		var node_name = node.get_name()
		if node_name.begins_with("SquareSpawn"): 
			SquareSpawnPoints.append(node)

func IncrementMobCount():
	total_mob_count += 1

func IncrementAstronautCount():
	num_astronauts_count += 1
	
func NextWave():
	#First we calculate how many astronauts remain
	if planet_disintegrated:
		RegeneratePlanet()
		
	
	max_landers += 1 #Once we launch this many, then there are no more for this round
	max_saucers += 1
	max_squares += 1
	max_pods += 1
	
	num_landers_launched = 0
	num_saucers_launched = 0
	num_squares_launched = 0
	num_pods_launched = 0
	
	cur_wave += 1
	#$HUD.ShowMessage("Attack wave " + str(cur_wave -1) +"\n completed.\n\n BONUS: \n" + str(CalculateBonus()) + "\n\nWave " + str(cur_wave))
	$HUD.ShowBonus("Attack wave " + str(cur_wave -1) +"\n completed. \nBonus x " + str((cur_wave -1)*100) + ":", num_astronauts_count, (cur_wave -1), "Wave " + str(cur_wave))
	SpawnInitMobs()
	$LanderSpawnTimer.start()
	$SaucerSpawnTimer.start()
	$SquareSpawnTimer.start()
	$SaucerPodTimer.start()
	
func RegeneratePlanet():
	print("RegeneratePlanet()")
	planet_disintegrated = false
	num_landers_launched = 0
	num_saucers_launched = 0
	num_squares_launched = 0
	total_mob_count = 0 
	num_astronauts_count = 0
	$HUD/Mountains/MountainsSprite.visible = true
	$Mountains.visible = true
	SpawnInitAstronauts()
	
func SpawnAstronaut():
	var new_astronaut = NewAstronaut.instance()
	self.add_child(new_astronaut)
	new_astronaut.position = Vector2(int(rand_range(350, 8500)), int(rand_range(450, 500)))
	new_astronaut.Dropped()
	num_astronauts_count += 1

func SpawnInitAstronauts():
	for i in range(1,init_astronaut_spawn):
		SpawnAstronaut()

func SpawnInitMobs():
	for i in range(1,init_lander_spawn):
		SpawnLander()
	for i in range(1,init_saucer_spawn):
		SpawnSaucer()
	for i in range(1,init_square_spawn):
		SpawnSquare()
	for i in range(1,init_pod_spawn):
		SpawnPod()
		
func SpawnLander():
	var spawn_point = randi() % 6
	#print("lander_spawn_point: " + str(spawn_point))
	var spawn_pos
	spawn_pos = LanderSpawnPoints[spawn_point].position
	var new_lander_path = LanderPath.instance()
	self.add_child(new_lander_path)
	new_lander_path.position = spawn_pos
	num_landers_launched += 1
	if num_landers_launched >= max_landers:
		$LanderSpawnTimer.stop()
	total_mob_count += 1

func SpawnMutant(pos):
	print("SpawnMutant(pos): " + str(pos))
	
	#Let's check to see if there was no position pass to us, if so, then we were called from a timer, and we need to randomly select a spawn pod
	if pos == Vector2(0,0):
		var spawn_point = randi() % 6
		#Use the lander spawn points
		pos = LanderSpawnPoints[spawn_point].position
	
	#When spawning mutants, use the position of the mob that we're replaceing to spawn a MutantPath2, which is more random than MutantPath1
	#If that mob position is too low or too high, shift it accordingly, so that the resulant MutantPath2 is mainly on-screen
	pos.x = clamp(pos.x, 200, Globals.PLAYER_TERRAIN_WIDTH * 0.9)
	pos.y = clamp(pos.y, 120, 150)
	
	var new_mutant_path = MutantPath.instance()
	self.add_child(new_mutant_path)
	new_mutant_path.position = pos
	num_landers_launched += 1 #We treat mutants as landers re: count
	total_mob_count += 1

func SpawnPod():
	var spawn_point = randi() % 6
	#print("lander_spawn_point: " + str(spawn_point))
	var spawn_pos
	spawn_pos = PodSpawnPoints[spawn_point].position
	var new_pod_path = PodPath.instance()
	self.add_child(new_pod_path)
	new_pod_path.position = spawn_pos
	num_pods_launched += 1
	if num_pods_launched >= max_pods:
		$PodSpawnTimer.stop()
	total_mob_count += 1

func SpawnSaucer():
	var spawn_point = randi() % 6
	#print("lander_spawn_point: " + str(spawn_point))
	var spawn_pos
	spawn_pos = SaucerSpawnPoints[spawn_point].position
	var new_saucer_path = SaucerPath.instance()
	self.add_child(new_saucer_path)
	new_saucer_path.position = spawn_pos
	num_saucers_launched += 1
	if num_saucers_launched >= max_saucers:
		$SaucerSpawnTimer.stop()
	total_mob_count += 1
	
func SpawnSquare():
	var spawn_point = randi() % 6
	#print("lander_spawn_point: " + str(spawn_point))
	var spawn_pos
	spawn_pos = SquareSpawnPoints[spawn_point].position
	var new_square_path = SquarePath.instance()
	self.add_child(new_square_path)
	new_square_path.position = spawn_pos
	num_squares_launched += 1
	if num_squares_launched >= max_squares:
		$SquareSpawnTimer.stop()
	total_mob_count += 1
	
func _on_LanderSpawnTimer_timeout():
	if planet_disintegrated:
		SpawnMutant(Vector2(0,0))
	else:
		SpawnLander()

func _on_PodSpawnTimer_timeout():
	if planet_disintegrated:
		SpawnMutant(Vector2(0,0))
	else:
		SpawnPod()

func _on_SaucerSpawnTimer_timeout():
	if planet_disintegrated:
		SpawnMutant(Vector2(0,0))
	else:
		SpawnSaucer()

func _on_SquareSpawnTimer_timeout():
	if planet_disintegrated:
		SpawnMutant(Vector2(0,0))
	else:
		SpawnSquare()

func _on_PlanetDisintegrateEffect_tween_completed(object, key):
	$Mountains.visible = false
	$Mountains.modulate = Color(1, 1, 1, 1)


func _on_PrintNPCTimer_timeout():
	for node in get_children():
		var node_name = node.get_name()
		if node_name.begins_with("Lander") or node_name.begins_with("Muant") or node_name.begins_with("Saucer") or node_name.begins_with("Square") or node_name.begins_with("Astronaut"): 
			print(node.get_name())


