extends Area2D

export var speed_x = 1
export var speed_y = 0

var speed = 400

var old_scale = Vector2()
var old_modulation
var phase_in = true

func _ready():
	old_modulation =  $Sprite.modulate
	old_scale = $Sprite.scale
	$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.4), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Sprite, "scale" , $Sprite.scale , $Sprite.scale * 1.4 , 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()

#func _process(delta):
#	var motion = Vector2(speed_x, speed_y) * speed
#	self.position = self.position + motion*delta

func _on_Tween_tween_completed(object, key):
	#print("_on_Tween_tween_completed(object, key)")
	$Timer.start()


func _on_Timer_timeout():
	if phase_in:
		$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 0.4), Color(1, 1, 1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.interpolate_property($Sprite, "scale" , $Sprite.scale , old_scale, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		phase_in = false
	else:
		$Tween.interpolate_property($Sprite, "modulate" , Color(1, 1, 1, 1), Color(1, 1, 1, 0.4), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.interpolate_property($Sprite, "scale" , old_scale , $Sprite.scale * 1.4 , 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		phase_in = true
	$Tween.start()


func _on_SquareBomb_area_entered(area):
	if area.get_groups().has("player"):
		$SelfDetonateTimer.start()


func _on_SelfDetonateTimer_timeout():
	queue_free()


func _on_FadeOutTimer_timeout():
	queue_free()
