extends KinematicBody2D

onready var Map = get_parent().get_parent().get_parent()
onready var HUD_Mountains = get_tree().get_root().get_node("Map").get_node("HUD").get_node("Mountains")

onready var NewAstronaut = preload("res://scenes/Astronaut.tscn")
onready var AstronautExplosion = preload("res://scenes/AstronautExplosion.tscn")
onready var MobExplosion = preload("res://scenes/MobExplosion.tscn")
onready var LanderBomb = preload("res://scenes/LanderBomb.tscn")
onready var RadarBlip = preload("res://scenes/LanderRadarBlip.tscn")
onready var Mutant = preload("res://scenes/MutantPath1.tscn")

export var active = false #Use this toggle to allow this node to be self-propelled, rather than relying on the Follow2D
export var speed = 100

enum MobAction { GRAB_ASTRONAUT = 0, SCANNING = 1, ASCENDING = 2, BUMPING = 3}

const SCORE = 150

var mob_action = MobAction.GRAB_ASTRONAUT

var direction = Vector2()
var velocity = Vector2()
var astronaut_pos = Vector2()
var astronaut_found = false
var astronaut_targeted = null
var astronaut_grabbed = false
var grabbing_astronaut = false
var mob_dying = false
var mob_hit = false
var hyperjump_out = false
var player_target_area
var can_shoot_bomb = false
var my_radar_blip
var drift_direction = 1 #Which direction to tend towards for the moment if active

func _ready():
	randomize()
	add_to_group("mob")
	add_to_group("lander")
	#firstly, we hide the sprites that show the lander and play the hyperjump animation first
	$SpriteRight.visible = false #Hide the sprite until hyperspace jump complete
	$SpriteLeft.visible = false #Hide the sprite until hyperspace jump complete
	$HyperspaceJump.connect("hyperspace", self, "_hyperspace_jump_finished")
	HyperjumpIn()
	
	#We spawn and show the radar blip
	SpawnRadarBlip()

func _process(delta):
	if active:
		#we are here because we're self propelled. 
		move_and_slide(direction.normalized() * speed)
		
		if astronaut_found and (astronaut_pos - self.global_position).length() < 10:
			#We have visited the spot where we thought there was an astronaut. There may not be one here now
			astronaut_found = false
			
			
	
	if can_shoot_bomb:
		#if it's time that we can shoot a bomb, let's shoot one.
		ShootBomb(player_target_area.global_position)
		can_shoot_bomb = false
	
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	#print("my_radar_blip.position: " + str(my_radar_blip.position))
	if astronaut_grabbed and self.global_position.y <= 100:
		#Lander becomes mutant
		BecomeMutant()
	
func BecomeMutant():
	print("BecomeMutant()")
	var new_mutant = Mutant.instance()
	Map.add_child(new_mutant)
	#print("lander_pos: " + str(self.global_position))
	new_mutant.position = self.global_position
	#print("new_mutant: " + str(new_mutant.position))
	self.visible = false
	#Map.DecrementAstronautCount() #We have to decrement the astronaut if we convert to mutant. This is already done in Astronaut.Free()
	#We don't call Free() here, as we don't want to decrement the mob_count in Map
	my_radar_blip.queue_free() #Get rid of the radar blip
	get_parent().get_parent().queue_free() #we free the lander path
	
	
func ChangeDirection():
	if !active:
		#We only do this if we're off our path leash
		return
	#print("ChangeDirection(): " + str(direction) + " mob_action: " + str(mob_action))
		
	#This is a state machine. 
	match mob_action:
		MobAction.SCANNING:
			#print("MobAction.SCANNING: " + str(global_position))
			if self.global_position.x <= 250:
				#print("self.position.x <= 50")
				direction.x = 1
				drift_direction = 1
			elif self.global_position.x >= Globals.PLAYER_TERRAIN_WIDTH - 250:
				#print("self.position.x >= Globals.PLAYER_TERRAIN_WIDTH")
				direction.x = -1
				drift_direction = -1
			else:
				#print("direction = Vector2(1,0)")
				direction.x = drift_direction
				
			if self.global_position.y >= 500:
				direction.y = -1
			elif self.global_position.y < 120:
				direction.y = 1
			else:
				direction.y = 0
				
		#On first being activated for self-propulsion, we have discovered an astronaut and going to bob down and grab him
		MobAction.GRAB_ASTRONAUT:
			#print("ChangeDirection(): GRAB_ASTRONAUT")
			if astronaut_found:
				direction =  astronaut_pos  - self.get_global_position()
				#print("astronaut_pos: %s global_position: %s direction: %s" % [astronaut_pos, self.get_global_position(), direction])
			else:
				mob_action = MobAction.SCANNING
				StartScanning()
	#If we've grabbed him, we move up
		MobAction.ASCENDING:
			#print("ChangeDirection(): ASCENDING")
			direction = Vector2(0, -1)
			
		MobAction.BUMPING:
			
			#HyperjumpOut()
			direction = Vector2(int(rand_range(-1, 1)), int(rand_range(-1, 1)))
			print("ChangeDirection(): BUMPING" + str(direction) + " mob_action: " + str(mob_action))
#			#direction = Vector2(1, -1)
			if astronaut_grabbed:
				mob_action = MobAction.ASCENDING
				#print("Set MobAction.ASCENDING")
			else:
				mob_action = MobAction.GRAB_ASTRONAUT
				#print("Set MobAction.GRAB_ASTRONAUT")
		_:
			StartScanning()
			if self.global_position.x <= 250:
				#print("self.position.x <= 50")
				direction.x = 1
				drift_direction = 1
			elif self.global_position.x >= Globals.PLAYER_TERRAIN_WIDTH - 250:
				#print("self.position.x >= Globals.PLAYER_TERRAIN_WIDTH")
				direction.x = -1
				drift_direction = -1
			else:
				#print("direction = Vector2(1,0)")
				direction.x = drift_direction
		
			if self.global_position.y >= 500:
				direction.y = -1
			elif self.global_position.y < 120:
				direction.y = 1
			else:
				direction.y = 0

func Free():
	Map.DecrementMobCount()
	my_radar_blip.queue_free() #Get rid of the radar blip
	get_parent().get_parent().queue_free() #we free the lander path

func GrabAstronaut(astronaut_body):
	#print("GrabAstronaut(): " + str(astronaut_body))
	#We have to free the body of the grabbed astronaut and show the one attached to us.
	astronaut_body.Free()
	$Astronaut.visible = true #Show the astronaut sprite on this node
	my_radar_blip.ShowAstronaut(true)
	#print("astronaut_body.position: " + str(astronaut_body.position)) 
	mob_action = MobAction.ASCENDING
	#print("Set MobAction.ASCENDING")
	astronaut_grabbed = true
	$GrabbedAustronautAudio.play()

func GrabbedAstronautHit():
	print("GrabbedAstronautHit()")
	$Astronaut.visible = false
	Map.DecrementAstronautCount() #The astronaut is gone, so we must decr the counter. 
	my_radar_blip.ShowAstronaut(false)
	astronaut_grabbed = false #astronaut no longer grabbed
	astronaut_targeted = false #no astronaut is targeted any more
	astronaut_found = false
	astronaut_targeted = null
	var new_expl = AstronautExplosion.instance()
	Map.add_child(new_expl)
	new_expl.position = self.global_position
	new_expl.emitting = true
	mob_action = MobAction.SCANNING
	StartScanning()
	ChangeDirection()
	
func HyperjumpIn():
	$HyperspaceJump.visible = true
	$HyperspaceJump/AnimationPlayer.play_backwards("HyperspaceJump")
	
func HyperjumpOut():
	$HyperspaceJump.visible = true
	hyperjump_out = true
	$HyperspaceJump/AnimationPlayer.play("HyperspaceJump")
	$SpriteLeft.visible = false
	$SpriteRight.visible = false
	
	
func MobDie():
	if !mob_dying:
		#Globals.blip_array[str(self)] = null #Globals.blip_array.erase()
		mob_dying = true
		var new_expl = MobExplosion.instance()
		Map.add_child(new_expl)
		new_expl.position = self.global_position
		new_expl.emitting = true
		#print("new_expl.position: " + str(new_expl.position))
		self.visible = false
		Globals.AddScore(SCORE)
		$ExplosionAudio.play()
		

func ReleaseAstronaut():
	#print("ReleaseAstronaut()")
	$Astronaut.visible = false #Hide the astronaut sprite on this node
	Map.IncrementAstronautCount() #As astronaut now released, we incrementn the count of them
	#Spawn a new astronaut node and drop if
	var new_astronaut = NewAstronaut.instance()
	Map.add_child(new_astronaut)
	new_astronaut.position = self.global_position + Vector2(-12,53)
	new_astronaut.Dropped()
	

func RoamLander():
	#Most likely called by the Follow2D path when the 
	if astronaut_found:
		$ScannerTimer.stop()
		$Scanner.enabled = false

func Scan():
	#print("Scan()")
	#We scan for astronauts
	if $Scanner.is_colliding():
		#print("lander scanner located something!")
		var collider = $Scanner.get_collider()
		#let's check to see if the collider still exists
		if collider != null:
			if collider.get_groups().has("astronaut"):
				astronaut_targeted = collider
				print("located astronaut: " + str(collider.get_name()))
				var collision_point = $Scanner.get_collision_point()
				astronaut_pos = collision_point - Vector2(0, +15) #We set the position for the top of the head of the last astronaut scanned
				#We have found an astronaut, so let's stop scanning.
				astronaut_found = true
				StopScanning()
				mob_action = MobAction.GRAB_ASTRONAUT

func StopScanning():
	#print("StopScanning()")
	$ScannerTimer.stop()
	$Scanner.enabled = false

func StartScanning():
	#print("StartScanning()")
	$ScannerTimer.start()
	$Scanner.enabled = true

func ShootBomb(target_pos):
	var target_dir = target_pos - self.global_position
	target_dir = target_dir.normalized()
	var bomb = LanderBomb.instance()
	bomb.global_position = self.global_position
	bomb.speed_x = target_dir.x 
	bomb.speed_y = target_dir.y
	bomb.add_to_group("bomb")
	#print("add_to_group(): " + str(self.get_instance_id()))
	Map.add_child(bomb)
	

func SpawnRadarBlip():
	#Let's add ourselves to the global array of blips to be monitored by the HUD radar
	#Globals.blip_array[str(self)] = self
	
	my_radar_blip = RadarBlip.instance()
	my_radar_blip.position.y = (self.global_position.y - 120) * Globals.vertical_scaling - 85
	my_radar_blip.position.x = (self.global_position.x * Globals.terrain_scaling) - 600
	#print("SpawnRadarBlip() x,y: " + str(my_radar_blip.position.x) + ", " + str(my_radar_blip.position.y))
	HUD_Mountains.add_child(my_radar_blip)
	

func _on_CycleTimer_timeout():
	if !hyperjump_out:
		$HyperspaceJump.visible = false
	
	if $SpriteLeft.visible:
		$SpriteLeft.visible = false
		$SpriteRight.visible = true
	else:
		$SpriteLeft.visible = true
		$SpriteRight.visible = false


func _on_ScannerTimer_timeout():
	Scan()
	


func _on_AreaDetection_area_entered(area):
	#print("Lander area entered1: " + str(area.get_name()))
	if area.get_groups().has("laser") and !mob_hit:
		#print("Lander hit by laser") 
		mob_hit = true #We shouldn't be shot again
		#we've been shot!
		if astronaut_grabbed:
			ReleaseAstronaut()
		MobDie()
		
	
	if area.get_groups().has("astronaut"):
		#print("1 area: " + str(area) + "astronaut_targeted: " + str(astronaut_targeted))
		if  str(astronaut_targeted) != "[Deleted Object]" and astronaut_targeted != null: #Make sure targeted astronaut still exists 
			#print("2 area: " + str(area) + "astronaut_targeted: " + str(astronaut_targeted))
			if area != null: #make sure astronaut we're touching still exists 
				#print("3 area: " + str(area) + "astronaut_targeted: " + str(astronaut_targeted))
				if !grabbing_astronaut and area.get_name() == astronaut_targeted.get_name():
					#print("4 area: " + str(area) + "astronaut_targeted: " + str(astronaut_targeted))
					grabbing_astronaut = true
					GrabAstronaut(astronaut_targeted)
					grabbing_astronaut = false
			
func _on_ExplosionAudio_finished():
#	print("_on_ExplosionAudio_finished()")
#	my_radar_blip.queue_free() #Get rid of the radar blip
#	get_parent().get_parent().queue_free() # we free the parent LanderPath node
	Free()
	
	
func _hyperspace_jump_finished():
	#Let's start the cycletimer so that the lander becomes visible
	$CycleTimer.start()
	#$HyperspaceJump.visible = false
	if hyperjump_out:
		#jump to new pos
		self.global_position = Vector2(int(rand_range(100, 8000)), int(rand_range(50, 450)))
		#print("HyperjumpOut() new_pos: " + str(self.global_position))
		hyperjump_out = false

func _on_AreaDetection_body_entered(body):
	#print("names: " + str(body) + str(self))
	if active and body.get_groups().has("mob") and str(body) != str(self): #Only do this when we're active/solo
		#print("pre-bumping direction: " + str(direction))
		mob_action = MobAction.BUMPING
		direction = -(self.position - body.position)
		print("Set MobAction.BUMPING")
		#ChangeDirection()


func _on_MovementTimer_timeout():
	if active:
		ChangeDirection()


func _on_PlayerScanner_area_entered(area):
	#print("_on_PlayerScanner_area_entered(area): " + str(area.get_name()))
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) player spotted")
		$ShootBombTimer.start()
		player_target_area = area

func _on_PlayerScanner_area_exited(area):
	if area.get_groups().has("player"):
		#print("_on_PlayerScanner_area_exited(area) left")
		#Player exited the scanner area, so stop targeting them
		$ShootBombTimer.stop()


func _on_ShootBombTimer_timeout():
	#print("_on_ShootBombTimer_timeout()")
	can_shoot_bomb = true
	
	

func _on_AstronautAreaDetection_area_entered(area):
	if $Astronaut.visible:
		if area.get_groups().has("laser"):
			print("Grabbed Astronaut hit by laser") 
			GrabbedAstronautHit()
