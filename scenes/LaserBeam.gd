extends Area2D

export var speed_x = 1
export var speed_y = 0

var speed = 1000

func _ready():
	add_to_group("laser")
	set_process(true)
	
func _process(delta):
	var motion = Vector2(speed_x, speed_y) * speed
	self.position = self.position + motion * delta


func _on_LaserBeam_body_entered(body):
	if body.get_groups().has("mob"):
		#print("laser hit: " + str(body))
		$HitTimer.start()

func _on_Timer_timeout():
	queue_free()


func _on_LaserBeam_area_entered(area):
	if area.get_groups().has("mob") or area.get_groups().has("astronaut"):
		#print("$HitTimer.start()")
		$HitTimer.start()

func _on_HitTimer_timeout():
	#print("_on_HitTimer_timeout()")
	queue_free()
