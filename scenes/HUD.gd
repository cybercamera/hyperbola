extends CanvasLayer

onready var NewLanderBlip = preload("res://scenes/LanderRadarBlip.tscn")
onready var NewAstronautBlip = preload("res://scenes/AstronautRadarBlip.tscn")
onready var BonusAstronaut = preload("res://scenes/BonusAstronaut.tscn")

onready var Player = get_parent().get_node("Player")
onready var Map = get_parent()



var blip_sprites = {}

var bonus_astronaut_count = 0
var bonus_astronaut_total = 0
var show_message_to_follow_bonus = ""

func _ready():
	
	Globals.screensize = Player.get_viewport_rect().size
	#print("Globals.screensize: " + str(Globals.screensize))
	#We establish a timer that checks to see if the Player has moved sufficiently to adjust the Mountains in the radar
	#$RadarScanTimer.start()
	#Lets compute the scaling ration of the HUD Radar to the actual full map.
	#terrain_scaling = Globals.HUD_TERRAIN_WIDTH/Globals.PLAYER_TERRAIN_WIDTH #<isn't working for some reaason
	Globals.terrain_scaling =  0.135 #0.132954545
	Globals.vertical_scaling = $Radar.texture.get_size().y/(Globals.screensize.y - $Radar.texture.get_size().y)
	print("terrain_scaling: %s vertical_scaling: %s " % [Globals.terrain_scaling, Globals.vertical_scaling])
	$Message.visible = false
	#Hide the smartbombs we aren't allocting presently
	$Smartbombs/Bomb7.visible = false
	$Smartbombs/Bomb6.visible = false
	$Smartbombs/Bomb5.visible = false
	$Smartbombs/Bomb4.visible = false
	#Hide the player lives we aren't allocting presently
	$PlayerLives/Life10.visible = false
	$PlayerLives/Life9.visible = false
	$PlayerLives/Life8.visible = false
	$PlayerLives/Life7.visible = false
	$PlayerLives/Life6.visible = false
	
	#Link to the globals file for score updates
	#Globals.connect("show_score", self, "ShowScore", score)
	$Bonus.visible = false
	ShowMessage("Wave 1")
	
	

func _process(delta):
	
	if Input.is_action_pressed("pause_toggle") and $PauseTimer.is_stopped():
		#Player selected to pause the game
		Globals.paused = not Globals.paused
		print("paused: " + str(Globals.paused))
		get_tree().set_pause(Globals.paused)
		ShowPauseMessage(Globals.paused)
	
	#Set the player sprite to point in the right direction and height
	if Player.get_node("SpriteRight").visible:
		$Radar/Player/SpriteRight.visible = true
		$Radar/Player/SpriteLeft.visible = false
	else:
		$Radar/Player/SpriteLeft.visible = true
		$Radar/Player/SpriteRight.visible = false
	
	#Figure out what vertical position to place tha radar player
	$Radar/Player.position.y = Player.position.y * Globals.vertical_scaling - 90
	#print("$Radar/Player.position.y: " + str($Radar/Player.position.y))
	#Figure out which 'segment' of the terrain map the player is in.
	#$Mountains.position.x = Globals.HUD_TERRAIN_WIDTH - (Player.position.x * (Globals.terrain_scaling * 0.6) + 250)
	$Mountains.position.x =  Globals.HUD_TERRAIN_WIDTH  - (Player.position.x *  Globals.terrain_scaling * 0.95 )  - 85
	
	#print("Player.position.x: " + str(Player.position.x) + "(Globals.PLAYER_TERRAIN_WIDTH  - Player.position.x ) *  Globals.terrain_scaling: " + str((Globals.PLAYER_TERRAIN_WIDTH  - Player.position.x ) *  Globals.terrain_scaling) + " $Mountains.position.x: " + str($Mountains.position.x))
	
func DumpArray(arr):
	for i in arr.keys():
		if arr[i] != null and str(arr[i]) != "[Deleted Object]":
			if !str(arr[i].get_class()).begins_with("KinematicBody2D") and !str(arr[i].get_class()).begins_with("Area2D"):
				print(str(arr[i].get_name()) + ": " + str(arr[i].get_class()))

func DecrementPlayerLives(player_count):
	#reduce the player count by 1
	match player_count:
		10:
			$PlayerLives/Life10.visible = false
		9:
			$PlayerLives/Life9.visible = false
		8:
			$PlayerLives/Life8.visible = false
		7:
			$PlayerLives/Life7.visible = false
		6:
			$PlayerLives/Life6.visible = false
		5:
			$PlayerLives/Life5.visible = false
		4:
			$PlayerLives/Life4.visible = false
		3:
			$PlayerLives/Life3.visible = false
		2:
			$PlayerLives/Life2.visible = false
		1:
			$PlayerLives/Life1.visible = false

func IncrementPlayerLives(player_count):
	print("IncrementPlayerLives(player_count)")
	#increase the player count by 1
	$PlayerLives/IncrementLives.play()
	match player_count:
		10:
			$PlayerLives/Life10.visible = true
		9:
			$PlayerLives/Life9.visible = true
		8:
			$PlayerLives/Life8.visible = true
		7:
			$PlayerLives/Life7.visible = true
		6:
			$PlayerLives/Life6.visible = true
		5:
			$PlayerLives/Life5.visible = true
		4:
			$PlayerLives/Life4.visible = true
		3:
			$PlayerLives/Life3.visible = true
		2:
			$PlayerLives/Life2.visible = true
		1:
			$PlayerLives/Life1.visible = true

func ShowScore(score):
	$Score.text = str("%7d" % score)
	
func ShowBonus(msg, num_astronauts, wave, show_message_to_follow):
	if $Bonus.visible == true:
		#We are here because the bonus machinery has already started, and we need to count up the bonus astronauts
		if bonus_astronaut_total <= 0:
			#We've counted down all the astronauts, so we stop the countdown timer
			$BonusAstronautTimer.stop()
			#We hide the Bonus machiner
			$Bonus.visible = false
			$Bonus.text = ""
			#Let's add the bonus to the score
			Globals.AddScore(bonus_astronaut_count * wave * 100)
			#And we show the ShowMessage
			ShowMessage(show_message_to_follow_bonus)
		else:
			#We spawn a new astronaut sprite and add it to the AstronautBonusBox
			bonus_astronaut_count += 1
			bonus_astronaut_total -= 1
			var new_astronaut = BonusAstronaut.instance()
			$Bonus/AstronautBonusBox.add_child(new_astronaut)
			new_astronaut.position = Vector2(bonus_astronaut_count * 20 + 10, 25)
	else: 
		$Bonus.visible = true
		$Bonus.text = msg
		bonus_astronaut_total = num_astronauts
		show_message_to_follow_bonus = show_message_to_follow
		$BonusAstronautTimer.start()
	
func ShowMessage(msg):
	$Message.text = msg
	$Message.visible = true
	$MessageAnimationPlayer.current_animation = "AnimateMessage"
	$MessageAnimationPlayer.play()
	$MessageTimer.start()

func ShowPauseMessage(show):
	$Message.text = "Game Paused"
	$Message.visible = show
	if show:
		$MessageAnimationPlayer.current_animation = "AnimateMessage"
		$MessageAnimationPlayer.play()
	else:
		$MessageAnimationPlayer.stop()
	$PauseTimer.start() #we start the pause timer during which time the player cannot unpause, to prevent key-bounce


func UpdateSmartBombs(num_bombs):
	match num_bombs:
		0:
			$Smartbombs/Bomb1.visible = false
			$Smartbombs/Bomb2.visible = false
			$Smartbombs/Bomb3.visible = false
			$Smartbombs/Bomb4.visible = false
			$Smartbombs/Bomb5.visible = false
			$Smartbombs/Bomb6.visible = false
			$Smartbombs/Bomb7.visible = false
		1:
			$Smartbombs/Bomb1.visible = true
			$Smartbombs/Bomb2.visible = false
			$Smartbombs/Bomb3.visible = false
			$Smartbombs/Bomb4.visible = false
			$Smartbombs/Bomb5.visible = false
			$Smartbombs/Bomb6.visible = false
			$Smartbombs/Bomb7.visible = false
		2:
			$Smartbombs/Bomb1.visible = true
			$Smartbombs/Bomb2.visible = true
			$Smartbombs/Bomb3.visible = false
			$Smartbombs/Bomb4.visible = false
			$Smartbombs/Bomb5.visible = false
			$Smartbombs/Bomb6.visible = false
			$Smartbombs/Bomb7.visible = false
		3:
			$Smartbombs/Bomb1.visible = true 
			$Smartbombs/Bomb2.visible = true
			$Smartbombs/Bomb3.visible = true
			$Smartbombs/Bomb4.visible = false
			$Smartbombs/Bomb5.visible = false
			$Smartbombs/Bomb6.visible = false
			$Smartbombs/Bomb7.visible = false
		4:
			$Smartbombs/Bomb1.visible = true 
			$Smartbombs/Bomb2.visible = true
			$Smartbombs/Bomb3.visible = true
			$Smartbombs/Bomb4.visible = true
			$Smartbombs/Bomb5.visible = false
			$Smartbombs/Bomb6.visible = false
			$Smartbombs/Bomb7.visible = false
		5:
			$Smartbombs/Bomb1.visible = true 
			$Smartbombs/Bomb2.visible = true
			$Smartbombs/Bomb3.visible = true
			$Smartbombs/Bomb4.visible = true
			$Smartbombs/Bomb5.visible = true
			$Smartbombs/Bomb6.visible = false
			$Smartbombs/Bomb7.visible = false
		6:
			$Smartbombs/Bomb1.visible = true 
			$Smartbombs/Bomb2.visible = true
			$Smartbombs/Bomb3.visible = true
			$Smartbombs/Bomb4.visible = true
			$Smartbombs/Bomb5.visible = true
			$Smartbombs/Bomb6.visible = true
			$Smartbombs/Bomb7.visible = false
		7:
			$Smartbombs/Bomb1.visible = true
			$Smartbombs/Bomb2.visible = true
			$Smartbombs/Bomb3.visible = true
			$Smartbombs/Bomb4.visible = true
			$Smartbombs/Bomb5.visible = true
			$Smartbombs/Bomb6.visible = true
			$Smartbombs/Bomb7.visible = true
			

func UpdateRadarBlip():
	#print("UpdateRadarBlip()")
	#print("size: " + str(Globals.blip_array.size()))
	if Globals.blip_array.empty():
		return
		
	for blip in Globals.blip_array.keys():
		#For each blip, check to see if we have it on the screen.
		#print("1. " + str(blip))
		if blip_sprites.has(blip):
			#we are here because this blip exists as a sprite. 
			if Globals.blip_array[blip] == null:
				#print("2. " + str(blip))
				#We are here because this blip was deleted. We have to delete the corresponding blip_sprite
				blip_sprites[blip].queue_free()
				blip_sprites.erase(blip)
				Globals.blip_array.erase(blip)
			else:
				print("3. " + str(blip) + " " + str(Globals.blip_array[blip]) + " " + Globals.blip_array[blip].get_class())
				#DumpArray(Globals.blip_array)
				if Globals.blip_array[blip] != null and str(Globals.blip_array[blip]) != "[Deleted Object]":
					if Globals.blip_array[blip].get_class().begins_with("Area2D") or Globals.blip_array[blip].get_class().begins_with("KinematicBody2D"): #Let's make sure it's the right kind of node
						blip_sprites[blip].position.y = Globals.blip_array[blip].global_position.y * Globals.vertical_scaling - 115
						blip_sprites[blip].position.x = Globals.blip_array[blip].global_position.x * (Globals.terrain_scaling * 0.6) - 420
						#print("blip_sprites pos: " + str(Vector2(blip_sprites[blip].position.x, blip_sprites[blip].position.y)))
		else:
			print("4. " + str(blip) + " " + str(Globals.blip_array[blip]))
			#we are here because this blip does not exist as a sprite. We must add it.
			if Globals.blip_array[blip] != null and str(Globals.blip_array[blip]) != "[Deleted Object]": #let's make sure that this wasn't deleted
				if Globals.blip_array[blip].get_groups().has("lander"):
					var radar_blip = NewLanderBlip.instance()
					$Mountains.add_child(radar_blip)
					radar_blip.position.y = Globals.blip_array[blip].global_position.y * Globals.vertical_scaling - 115
					radar_blip.position.x = Globals.blip_array[blip].global_position.x * (Globals.terrain_scaling * 0.6) - 420
					blip_sprites[blip] = radar_blip
				elif Globals.blip_array[blip].get_groups().has("astronaut"):
					#print("has astronaut")
					var radar_blip = NewAstronautBlip.instance()
					$Mountains.add_child(radar_blip)
					radar_blip.position.y = Globals.blip_array[blip].global_position.y * Globals.vertical_scaling - 115
					radar_blip.position.x = Globals.blip_array[blip].global_position.x * (Globals.terrain_scaling * 0.6) - 420
					blip_sprites[blip] = radar_blip
				


func _on_MessageTimer_timeout():
	$Message.visible = true
	$Message.text = ""
	$MessageAnimationPlayer.stop()
	


func _on_BonusAstronautTimer_timeout():
	ShowBonus("",0, 0, "")
