extends Path2D

onready var Map = get_parent()
export var mob_speed = 250

var free_path = false

#This node doesn't need to be added to mob_path as its constituents wont be converted to mutants
#func _ready():
#	add_to_group("mob_path")
	
func _process(delta):
	$Follow.set_offset($Follow.get_offset() + mob_speed * delta)

#func GetMobPosition():
#	#We need to find the exact position of the mob held by this path
#	return $Follow/Mutant.global_position