extends Particles2D

signal player_explosion_finished

func _ready():
	emitting = true
	$AnimationPlayer.play("Embers")


func _on_AnimationPlayer_animation_finished(anim_name):
	#print("Player_Explosion_animation_finished")
	emit_signal("player_explosion_finished")
	queue_free()
