extends Path2D

onready var Map = get_parent()
export var lander_speed = 100

var free_path = false

func _ready():
	add_to_group("mob_path")
	randomize()
	$Timer.wait_time = randi() % 20 + 1 #We set a random time for the timer action
	$Follow.unit_offset = randf() #we set a random point of location of this mob on the follow path, between 0 & 1
	
func _process(delta):
	$Follow.set_offset($Follow.get_offset() + lander_speed * delta)

func Free():
	$Follow/Lander.Free()
	
func GetMobPosition():
	#We need to find the exact position of the mob held by this path
	return $Follow/Lander.global_position

func _on_Timer_timeout():
	print("detaching lander from path")
	var lander = get_node("Follow").get_node("Lander")
	self.remove_child(lander)
	Map.add_child(lander)
	if lander != null:
		lander.active = true #Make the lander self-propelled & directed
	set_process(false) #Let's stop processing this