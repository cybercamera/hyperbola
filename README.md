#Hyperbola Homepage - https://cybercamera.gitlab.io/hyperbola-homepage/

**Note** *This game is currently in Most of it works. Please log any issues with the issues tracker on this GitLab repository page*

![Hyperbola Main Menu](main_menu.png?raw=true "Hyperbola Main Menu")

Hyperbola is a game inspired by perhaps the greatest arcade game of the golden period of arcade games:  Defender!

To play Hyperbola, use the W, A, S and D keys or the arrow keys for movement, the SPACE bar to shoot, the Control key for hyperspace, the ALT key for smartbombs and P to pause the game.

![Hyperbola Game in action](game.png?raw=true "Hyperbola game in action")

The aim of the game is to defend the astronauts found on the surface of the planet from the marauding aliens. Alien landers will try to grab the astronauts and drag them into space, thus turning them into hyperactive mutants. If an astronaut is grabbed, you must shoot the alien lander and catch the falling astronaut, if the're too high up to land safely back on the planet, all while dodging the variety of alien life-forms which are zooming around. 

Once you completely clear out each wave of aliens, another wave will materialise. Be warned, if all astronauts are lost, the planet will explode and all remaining aliens will become mutants. You must then destroy all of these before a new planet with new astronauts becomes available to you.


Hyperbola is licenced under the GNU Public Licence v3. Hyperbola is available for download from: 

https://cybercamera.gitlab.io/hyperbola-homepage/


Or, you may be able to play the game here:

https://cybercamera.gitlab.io/hyperbola-homepage/binaries/Hyperbola.html
